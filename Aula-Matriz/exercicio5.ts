/*Crie uma matriz 4x4 com números aleatórios entre 0 e 50. 
Em seguida, crie um código que calcule e mostre a 
quantidade de valores pares encontrados na matriz. */

namespace exercicio5{
    let matriz: number[][] = Array.from({length: 4}, () => Array(4).fill(0));

    for (let i = 0; i < matriz.length; i++) {
        for (let j = 0; j < matriz[i].length; j++) {
            matriz[i][j] = Math.floor(Math.random() * 51);
        }
    }
    
}