namespace introFOR 
{
    for(let i = 0; i < 3; i++)
    {
        console.log("Bloco" + i);
    }
    //mostrar todos os numeros impares
    for(let i = 0; i <= 10; ++i)
    {
        if(i % 2 != 0)
        {
            console.log(`O número ${i} é impar`);
        }
    }

    //mostrar a tabuada do 2
    let n = 8;
    for(let i = 0; i < 11; i++)
    {
        console.log(`${i} * ${n} = ${i*n}`);
    }
}