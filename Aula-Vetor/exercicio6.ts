/*Crie um vetor chamado "alunos" contendo três objetos, cada um representando 
um aluno com as seguintes propriedades: "nome" (string), "idade" (number) e
"notas" (array de números). Preencha o vetor com informações fictícias.
em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno,
calcule a média das notas e imprima o resultado na tela, juntamente com
o nome e a idade do aluno. */

    interface Aluno {
        nome : string;
        idade : number;
        notas : number[];
    }

namespace exercicio6 {
    const alunos: Aluno[] = [
        {nome: "Aluno 1", idade: 20, notas: [4,7,8]},
        {nome: "Aluno 2", idade: 28, notas: [6,10,4]},
        {nome: "Aluno 3", idade: 19, notas: [8,6,10]},
        {nome: "Aluno 4", idade: 32, notas: [9,7,8]},
        {nome: "Aluno 5", idade: 25, notas: [4,9,5]},
        {nome: "Aluno 6", idade: 18, notas: [8,7,6]}
    ]

    /*alunos.forEach((aluno) => {
        console.log("-----------------------------------------------")
        console.log(aluno.nome);
        console.log((aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) /3);
    })*/

    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce((total, nota) => {
            return total + nota
        }) / aluno.notas.length
        if (media >= 7) {
            console.log(`A média do alun ${aluno.nome} é igual à ${media} e ele está aprovado.`);
        } else {
            console.log (`A média do aluno ${aluno.nome} é igaul à ${media} e ele não está aprovado.`)
        }
    })

}
