/*Crie um array com 4 objetos, cada um representando um livro com 
as propriedades titulo e autor. Em seguida, use o método map() 
para criar um novo array contendo apenas os títulos dos livros.
*/
namespace exercicio3{
    let livros: any[] = [
        {titulo: "A paciente silenciosa", autor: "Autor1"},
        {titulo:"A biblioteca da meia-noite", autor:"Autor2"},
        {titulo:"Jantar Secreto", autor:"Autor3"},
        {titulo:"Suicidas", autor:"Autor3"}
    ];

    let titulos = livros.map((livros) => {
        return livros.titulo
    });
    
    let autores = livros.map((livros) => {
        return livros.titulo
    })

    console.log(autores);
    console.log(titulos);

    /*Dado um array de objetos livros, contendo os campos titulo e autor, 
    crie um programa em TypeScript que utilize a função filter() para 
    encontrar todos os livros do autor com valor "Autor 3". 
    Em seguida, utilize a função map() para mostrar apenas 
    os títulos dos livros encontrados. O resultado deve 
    ser exibido no console.*/
    let livrosAutor3 = livros.filter((livro) => {
        return livro.autor === "Autor3"
    })

    console.log(livrosAutor3);

    let titulosAutor3 = livrosAutor3.map((livro) => {
        return livro.titulo;
    })
    console.log(titulosAutor3);
}
