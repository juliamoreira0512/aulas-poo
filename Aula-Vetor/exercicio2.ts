/*Crie um array com 3 nomes de frutas. Em seguida, use um loop while
para iterar sobre o array e exibir cada fruta em uma linha separada.
*/
namespace exercicio2 {
    let frutas: string[] = ['morango', 'banana', 'laranja'];
    //console.log(frutas[0]);
    //console.log(frutas[1]);
    //console.log(frutas[2]);

    let i: number = 0;
    while (i < frutas.length) {
        console.log(frutas[i]);
        i++;
}
}