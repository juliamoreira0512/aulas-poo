/*Crie um array com 6 números. Em seguida, use o método filter() 
para criar um novo array contendo apenas os números ímpares*/
namespace exercicio4{
    let numeros: number[] = [9, 56, 34, 3, 17, 48];
    let impares = numeros.filter(function(num) {
        return num % 2 === 1;
    });

    console.log(impares);
}