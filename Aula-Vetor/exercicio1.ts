/*Crie um array com 5 números. Em seguida, use um loop for 
para iterar sobre o array e exibir a soma de todos os números.
*/
namespace exercicio1 {
    let numeros: number[] = [5, 9, 2, 10, 78];
    let somaNumeros: number = 0;

    for (let i = 0; i < numeros.length; i++)
    {
        somaNumeros = somaNumeros + numeros[i];
        //somaNumeros += numeros[i];
    }
    let result: number;
    result = somaNumeros
    console.log (`A soma final é: ${result}` );

    //Criando uma iterção com multiplicação
    let multi: number = 1;
    for (let i = 0 ; i < numeros.length ; i++)
    {
        multi *= numeros[i];
}

console.log(`O resultado da multiplicação é: ${multi}`);
    }