/*Escreva um programa que pergunte ao usuário qual o seu sabor de 
sorvete favorito e exiba uma mensagem de acordo com o sabor escolhido
*/
namespace exercicio3 {
    console.log("Qual seu sabor de sorvete favorito?")
    let saborFavorito: string = "creme";

    switch(saborFavorito) {
        case "chocolate": console.log("Hmmm esse é bom.");
                break;
        case "creme": console.log("Nossa! Essa é meu preferido também!");
                break;
        case "morango": console.log("Adoro esse, o doce e o azedo na medida certa.");
                break;
        case "limão": console.log("Bem azedo esse, não?");
                break;
        case "coco": console.log("Esse é uma delícia, principalmente com os pedaços da fruta.");
                break;
        case "flocos": console.log("Esse é bom, mas prefiro outros.");
                break;
        case "açaí": console.log("Açaí é melhor que sorvete, verdade.");
                break;
        case "céu azul": console.log("Esse é bem gosto, mas bem doce");
                break;
        default: console.log("Esse sabor não consta em nosso sistema!");
    }
}