/*Escreva um programa que pergunte ao usuário qual a sua cor favorita
e exiba uma mensagem de acordo com a cor escolhida:
*/
namespace exercicio2 
{
    console.log("Qual sua cor preferida?");
    let cor: string = "roxo"

    switch(cor) {
        case "azul": console.log("Esta cor traz um sentimento de contemplação, paz, paciência, emoções mais amenas e leves.");
                    break;
        case "vermelho": console.log("Esta cor traz um sentimento de paixão, fúria, violência, fome, todo tipo de sensação intensa.");
                    break;
        case "verde": console.log("Esta cor traz um sentimento de cura, perseverança, natureza");
                    break;
        case "amarelo": console.log("Esta cor traz um sentimento de alegria, relaxamento, felicidade");
                    break;
        case "laranja": console.log("Esta cor traz um sentimento de bom humor, energia, equilíbrio.");
                    break;
        case "roxo": console.log("Esta cor traz um sentimento de sensualidade, nobreza, mistério, transformação, a cor das descobertas.");
                    break;
        default: console.log("Esta cor não consta em nosso sistema!");
    }

}